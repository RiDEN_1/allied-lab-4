import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Task1 {

    private ChromeDriver driver;
    private Select countrySelect;
    private Select monthSelect;
    private Select daySelect;
    private Select yearSelect;
    private WebElement firstName;
    private WebElement lastName;
    private WebElement maritalStatus;
    private WebElement maritalStatus2;
    private WebElement maritalStatus3;
    private WebElement hobby;
    private WebElement hobby2;
    private WebElement hobby3;
    private WebElement phoneNumber;
    private WebElement username;
    private WebElement email;
    private WebElement profilePicture;
    private WebElement aboutYourself;
    private WebElement password;
    private WebElement confirmPassword;
    private WebElement submitButton;

    @BeforeTest
    public void setup() {
        System.setProperty("webdriver.chrome.driver", "D:\\University\\6semester\\APPOO\\automation\\chromedriver.exe");
        driver=new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://demoqa.com/registration/");
    }

    @BeforeMethod
    public void beforeMethod() {
        WebElement country = driver.findElement(By.xpath("//select[@id='dropdown_7']"));
        countrySelect = new Select(country);
        WebElement month = driver.findElement(By.xpath("//select[@id='mm_date_8']"));
        monthSelect = new Select(month);
        WebElement day = driver.findElement(By.xpath("//select[@id='dd_date_8']"));
        daySelect = new Select(day);
        WebElement year = driver.findElement(By.xpath("//select[@id='yy_date_8']"));
        yearSelect = new Select(year);

        firstName = driver.findElement(By.xpath("//input[@id='name_3_firstname']"));
        lastName = driver.findElement(By.xpath("//input[@id='name_3_lastname']"));
        maritalStatus = driver.findElement(By.xpath("//input[@name='radio_4[]' and @value='single']"));
        maritalStatus2 = driver.findElement(By.xpath("//input[@name='radio_4[]' and @value='married']"));
        maritalStatus3 = driver.findElement(By.xpath("//input[@name='radio_4[]' and @value='divorced']"));
        hobby = driver.findElement(By.xpath("//input[@data-map-field-by-class='checkbox_5' and @value='dance']"));
        hobby2 = driver.findElement(By.xpath("//input[@data-map-field-by-class='checkbox_5' and @value='reading']"));
        hobby3 = driver.findElement(By.xpath("//input[@data-map-field-by-class='checkbox_5' and @value='cricket ']"));
        phoneNumber = driver.findElement(By.xpath("//input[@id='phone_9']"));
        username = driver.findElement(By.xpath("//input[@id='username']"));
        email = driver.findElement(By.xpath("//input[@id='email_1']"));
        profilePicture = driver.findElement(By.xpath("//input[@type='file']"));
        aboutYourself = driver.findElement(By.xpath("//textarea[@id='description']"));
        password = driver.findElement(By.xpath("//input[@id='password_2']"));
        confirmPassword= driver.findElement(By.xpath("//input[@id='confirm_password_password_2']"));
        submitButton = driver.findElement(By.xpath("//input[@type='submit']"));
    }

    @Test
    public void testRegistrationPage() throws InterruptedException {
        firstName.clear();
        firstName.sendKeys("Test1Name");
        lastName.clear();
        lastName.sendKeys("Test1SurName");
        maritalStatus.click();
        hobby.click();
        countrySelect.selectByVisibleText("Afghanistan");
        monthSelect.selectByVisibleText("2");
        daySelect.selectByVisibleText("2");
        yearSelect.selectByVisibleText("1998");
        phoneNumber.clear();
        phoneNumber.sendKeys("1234567891");
        username.clear();
        username.sendKeys("TestloUserName1");
        email.clear();
        email.sendKeys("Test1lol@gmail.com");
        profilePicture.sendKeys("D:\\University\\6semester\\APPOO\\automation\\testimage.jpg");
        aboutYourself.clear();
        aboutYourself.sendKeys("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent varius"+
                "augue quis lacus eleifend, ut iaculis nulla ultricies. Sed dictum dictum suscipit. Sed diam orci, placerat ut lacus et, rhoncus convallis elit.");
        password.clear();
        password.sendKeys("SAFlbakenbl243rsdv");
        confirmPassword.clear();
        confirmPassword.sendKeys("SAFlbakenbl243rsdv");
        submitButton.click();

        Assert.assertTrue(driver.findElement(By.xpath("//*[@id=\"post-49\"]/div/p")).getText().equals("Thank you for your registration"));
        Thread.sleep(10000);
    }

    @Test
    public void testRegistrationPage2() throws InterruptedException {
        firstName.clear();
        firstName.sendKeys("Test2Name");
        lastName.clear();
        lastName.sendKeys("Test2SurName");
        maritalStatus.click();
        hobby.click();
        countrySelect.selectByVisibleText("Australia");
        monthSelect.selectByVisibleText("3");
        daySelect.selectByVisibleText("3");
        yearSelect.selectByVisibleText("1997");
        phoneNumber.clear();
        phoneNumber.sendKeys("9876543219");
        username.clear();
        username.sendKeys("TestloUserName2");
        email.clear();
        email.sendKeys("Test2lol@gmail.com");
        profilePicture.sendKeys("D:\\University\\6semester\\APPOO\\automation\\testimage.jpg");
        aboutYourself.clear();
        aboutYourself.sendKeys("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent varius"+
                "augue quis lacus eleifend, ut iaculis nulla ultricies. Sed dictum dictum suscipit. Sed diam orci, placerat ut lacus et, rhoncus convallis elit.");
        password.clear();
        password.sendKeys("sDSFI1231dsf5sew");
        confirmPassword.clear();
        confirmPassword.sendKeys("sDSFI1231dsf5sew");
        submitButton.click();

        Assert.assertTrue(driver.findElement(By.xpath("//*[@id=\"post-49\"]/div/p")).getText().equals("Thank you for your registration"));
        Thread.sleep(10000);
    }

    @Test
    public void testRegistrationPage3() throws InterruptedException {
        firstName.clear();
        firstName.sendKeys("Test3Name");
        lastName.clear();
        lastName.sendKeys("Test3SurName");
        maritalStatus.click();
        hobby.click();
        countrySelect.selectByVisibleText("Morocco");
        monthSelect.selectByVisibleText("6");
        daySelect.selectByVisibleText("14");
        yearSelect.selectByVisibleText("1988");
        phoneNumber.clear();
        phoneNumber.sendKeys("9638527419");
        username.clear();
        username.sendKeys("TestloUserName3");
        email.clear();
        email.sendKeys("Test3kek@gmail.com");
        profilePicture.sendKeys("D:\\University\\6semester\\APPOO\\automation\\testimage.jpg");
        aboutYourself.clear();
        aboutYourself.sendKeys("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent varius"+
                "augue quis lacus eleifend, ut iaculis nulla ultricies. Sed dictum dictum suscipit. Sed diam orci, placerat ut lacus et, rhoncus convallis elit.");
        password.clear();
        password.sendKeys("AVLesrj124SFDMqw");
        confirmPassword.clear();
        confirmPassword.sendKeys("AVLesrj124SFDMqw");
        submitButton.click();

        Assert.assertTrue(driver.findElement(By.xpath("//*[@id=\"post-49\"]/div/p")).getText().equals("Thank you for your registration"));
        Thread.sleep(10000);
    }

    @AfterTest
    public void close() {
        driver.close();
    }

}
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class Task2 {

    private ChromeDriver driver;
    private Select lotSelect;
    private WebElement timeFrom;
    private WebElement timeFromPm;
    private WebElement timeFromAm;
    private WebElement dateFrom;
    private WebElement timeTo;
    private WebElement timeToPm;
    private WebElement timeToAm;
    private WebElement dateTo;
    private WebElement calcBtn;
    private WebElement costTxt;
    private By submitBy = By.xpath("//tr[./td/div[text()='COST']]/td/span[@class='SubHead']//b");


    @DataProvider(name = "parkingCalculator")
    public Object[][] getData() {
        return new Object[][]{
                {"Short-Term Parking", "9:00", "05/23/2018", "22:00", "05/23/2018", "$ 26.00"},
                {"Economy Parking", "13:00", "05/23/2018", "20:00", "05/23/2018", "$ 9.00"},
                {"Long-Term Surface Parking", "8:00", "05/23/2018", "12:00", "05/23/2018", "$ 8.00"},
                {"Long-Term Garage Parking", "8:00", "05/23/2018", "20:00", "05/23/2018", "$ 12.00"},
                {"Valet Parking", "8:00", "05/23/2018", "17:00", "05/23/2018", "$ 30.00"}
        };

    }

    @BeforeTest
    public void setup() {
        System.setProperty("webdriver.chrome.driver", "D:\\University\\6semester\\APPOO\\automation\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://adam.goucher.ca/parkcalc/");
    }

    @BeforeMethod
    public void beforeMethod() {
        WebElement lot = driver.findElement(By.xpath("//select[@id='Lot']"));
        lotSelect = new Select(lot);
        timeFrom = driver.findElement(By.id("EntryTime"));
        timeFromPm = driver.findElement(By.xpath("//input[@name='EntryTimeAMPM' and @value='PM']"));
        timeFromAm = driver.findElement(By.xpath("//input[@name='EntryTimeAMPM' and @value='AM']"));
        dateFrom = driver.findElement(By.xpath("//input[@id='EntryDate']"));
        timeTo = driver.findElement(By.id("ExitTime"));
        timeToPm = driver.findElement(By.xpath("//input[@name='ExitTimeAMPM' and @value='PM']"));
        timeToAm = driver.findElement(By.xpath("//input[@name='ExitTimeAMPM' and @value='PM']"));
        dateTo = driver.findElement(By.xpath("//input[@id='ExitDate']"));
        calcBtn = driver.findElement(By.xpath("//input[@type='submit']"));
        costTxt = driver.findElement(By.xpath("//span[@class='SubHead']//b"));

    }
    private void clickAmPm(String timeValue, WebElement timeAm, WebElement timePm, WebElement timeSend) {
        SimpleDateFormat dtf = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
        SimpleDateFormat dtfa = new SimpleDateFormat("hh:mm aa", Locale.ENGLISH);
        SimpleDateFormat dtfs = new SimpleDateFormat("hh:mm", Locale.ENGLISH);
        try {
            Date date = dtf.parse(timeValue);
            String result = dtfa.format(date);
            if (result.toLowerCase().contains("am")) {
                timeAm.click();
                timeSend.sendKeys(timeValue);
            } else {
                timePm.click();
                timeSend.sendKeys(dtfs.format(date));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
    @Test(dataProvider = "parkingCalculator")
    public void testPostsPage(String lotSelectValue, String timeFromValue, String dateFromValue,
                              String timeToValue, String dateToInputValue, String expectedCost) throws InterruptedException {

        lotSelect.selectByVisibleText(lotSelectValue);
        timeFrom.clear();
        clickAmPm(timeFromValue, timeFromAm, timeFromPm, timeFrom);
        dateFrom.clear();
        dateFrom.sendKeys(dateFromValue);
        timeTo.clear();
        clickAmPm(timeToValue, timeToAm, timeToPm, timeTo);
        dateTo.clear();
        dateTo.sendKeys(dateToInputValue);
        calcBtn.click();
        costTxt = driver.findElement(submitBy);

        Assert.assertEquals(costTxt.getText(), expectedCost, "Parking cost");
        //Thread.sleep(10000);
    }
    @AfterTest
    public void close() {
        driver.close();
    }

}